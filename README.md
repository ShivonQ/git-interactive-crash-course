# Git Crash Course

_What it is, and what it isn't._

## Overview

Welcome to the Comprehensive Git Learning Repository! This project is designed to provide a hands-on learning experience for understanding and mastering Git, the widely used distributed version control system. The goal is to offer both theoretical knowledge and practical skills through a series of interactive lessons and exercises.

## How to Use This Repository

1. **Fork the Repository:** Start by forking this repository to your own account. This will be your personal workspace for all exercises.
2. **Clone Your Fork:** Clone the forked repository to your local machine to begin working with the files.
3. **Navigate Through Lessons:** The repository is organized into lesson directories. Each directory contains a Markdown file with instructions and exercises related to a specific aspect of Git.
4. **Complete the Exercises:** As you progress through each lesson, you'll find hands-on exercises designed to reinforce the concepts covered. Follow the instructions in each lesson to complete these exercises.
5. **Push Your Changes:** After completing each exercise, commit and push your changes to your forked repository. This will give you practical experience with Git commands.

## Official Git Documentation

- [Git Official Documentation](https://git-scm.com/doc)
- [Pro Git Book](https://git-scm.com/book/en/v2)

## Lessons Directory

- `01-Introduction-to-Git`: Basics and overview of Git.
- `02-Setting-Up-Git`: Instructions on installing and configuring Git on various platforms.
- ... _(add to this as we go...)_

## Resources

- **Cheat Sheets:**
  - [GitLab Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
  - [Atlassian Git Cheat Sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)

## Contribution Guidelines

If you would like to contribute to this repository, please tell Malcolm.

## Feedback and Support

We encourage users to open [GitLab Issues](https://gitlab.com/bluestem/toolbox/git-crash-course/-/issues) for feedback, suggestions, or any Git-related queries you might have during your learning journey.

## License

This project is licensed under the [MIT License](./LICENSE).

## Acknowledgments

_We acknowledge no kings, no masters._
