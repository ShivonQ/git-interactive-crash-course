# Lesson 1: Introduction to Git

[[_TOC_]]

## Introduction 🌟

Welcome to the first lesson of our Git learning journey! 🚀 This lesson is designed to introduce you to Git, a powerful tool that has become a cornerstone of modern software development. We'll cover what Git is, its importance in the software development process, and the basic concepts you need to understand as you start using Git.

## What is Git? 🤔

Git is a distributed version control system (DVCS), often just referred to as a VCS, that helps in tracking changes in source code during software development. Unlike centralized version control systems, Git gives every developer their own local repository with a full history of commits.

![A visual depiction of the basic git flow](/assets/git-flow-chart.svg)

## Importance of Version Control 💡

Version control, crucial in software development, enables teams to manage code changes over time. Particularly in large projects, version control systems like Git allow multiple developers to work on different aspects simultaneously, merging their changes into a cohesive final product.

### Facilitating Teamwork and Collaboration 🤝

Version control enhances teamwork in several ways:

- **Concurrent Development:** 🛠 It supports simultaneous work on different project parts, with each developer working in isolated branches to avoid disruptions.
- **Tracking Contributions:** 📊 Changes are tracked with details on who made them and why, aiding in understanding project evolution and ensuring accountability.
- **Resolving Conflicts:** 🔧 VCS provides conflict resolution tools, maintaining code integrity when merging simultaneous changes.
- **Review and Feedback:** 💬 It supports code reviews and discussions before merging, improving code quality and knowledge sharing.

### Additional Benefits 🌈

- **History and Documentation:** 📚 Version control logs every change, offering a detailed project history and the ability to revert to previous states if needed.
- **Branching and Merging:** 🔀 Advanced branching and merging features facilitate experimental development, feature branches, and separate release management.
- **Risk Reduction:** ⚠️ A comprehensive change record and revert options significantly lower the risk of data loss or critical bugs.
- **Integration:** 🤖 Modern version control integrates with various tools and automation systems, enhancing workflow efficiency.

## Basic Git Terminology 📘

Understanding Git's basic terminology is crucial for grasping its functionalities. Let's go over some fundamental terms you will encounter frequently.

### Repository 📁

A repository in Git, often referred to as a 'repo', is the most fundamental element. It's where the source code and its history are stored. [Learn more about Repositories](https://git-scm.com/docs/git-repository).

### Branch 🌿 `git branch`

Branches in Git are essentially pointers to a snapshot of your changes. They allow you to diverge from the main line of development and work independently without affecting other branches. [Learn more about Branching](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell).

### Commit ✍️ `git commit -m "I fixed that thing!"`

A commit in Git represents a snapshot of your repository at a specific point in time, often containing a brief description of the changes and a unique identifier. [Learn more about Commits](https://git-scm.com/docs/git-commit).

### Merge 🔀 `git merge`

Merging is a way to integrate changes from one branch into another. It's a common Git operation that allows you to combine multiple sequences of commits into one unified history. [Learn more about Merging](https://git-scm.com/docs/git-merge).

## Git vs. Platforms like GitHub and GitLab 🌐

It's important to distinguish between Git itself and web-based hosting services for version control such as GitHub and GitLab.

### What is Git? 🛠

Git is a distributed version control system that operates independently of any online platform. It enables users to track changes in their code across different files and coordinate work among multiple people. Primarily used for source code management in software development, Git allows for multiple versions of a project to be maintained simultaneously.

### No Really, what is Git?

When you modify a file in your repository, the change is initially unstaged. In order to `commit` it, you must `stage` it, _that is to add it to the index_, using `git add`. When you make a `commit`, the changes that are committed are those that have been added to the `index`.

`git reset` changes, at minimum, where the current branch (`HEAD`) is pointing. This however is a more advanced topic and is in the third lesson of this crash-course.

A visualization of the movements on the git tree, showing how the most common commands function in relation to "Staging/Index" and other components of the actual mechanics of git.

![A visualization of the movements on the git tree, showing how the most common commands function in relation to "staging/Index" and other components of the actual mechanics of git.](/assets/git-actions.jpeg)

### What are GitHub and GitLab? 🏢

GitHub and GitLab are web-based platforms that host Git repositories and provide additional features for project management. GitHub, a subsidiary of Microsoft, is known for its collaborative features, issue tracking, and robust community support. GitLab, on the other hand, offers a similar set of features but also includes its own continuous integration and deployment (CI/CD) tools, making it a comprehensive DevOps platform.

### Differences and Use Cases 🤔

While Git is a tool for version control, GitHub and GitLab are hosting services that provide a user interface and additional tools for managing Git repositories. Git helps in tracking and managing code changes, whereas GitHub and GitLab offer collaborative features like pull requests, issue tracking, and wikis. They complement Git by providing a visual and collaborative layer on top of Git's functionality, enhancing teamwork and project management in software development.

---

## Hands-On Exercise: Setting Up and Exploring Git

To get a practical feel of Git, let's set it up on your machine and explore some basic commands.

### Installing Git

- **For Windows:** Download and install Git from [Git for Windows](https://gitforwindows.org/). During installation, you can select the default options.
- **For Mac:** Install Git using Homebrew by running `brew install git` in the terminal. If you don't have Homebrew, install it from [here](https://brew.sh/).
- **For Linux:** Use your distribution's package manager, for example, on Ubuntu, run `sudo apt-get install git`.

### Exploring Git

After installing Git, perform the following tasks using this repository:

- **Task 1: Configure Git**  
  Configure your Git installation with your name and email. This information will be included in your commits.

  ```bash
  git config --global user.name "Your Name"
  git config --global user.email "your.email@example.com"
  ```

- **Task 2: Fork and Clone the Repository**  
  Fork this repository to your account and then clone it to your local machine. Use the clone command:

  ```bash
  git clone [URL of your forked repository]
  ```

- **Task 3: Create a New Branch**  
  Create and switch to a new branch named `practice-branch`.

  ```bash
  git branch practice-branch
  git checkout practice-branch
  ```

- **Task 4: Make Changes and Commit**  
  Make a small change in a file (e.g., add a note in one of the lesson files). Then add and commit the change.

  ```bash
  git add .
  git commit -m "Add my first change"
  ```

- **Task 5: Push Changes and Check Status**  
  Push your changes to the forked repository on GitHub and then check the status of your repository.

  ```bash
  git push origin practice-branch
  git status
  ```

- **Task 6: Explore Commit History**  
   Use `git log` to view the commit history of the branch.
  ```bash
  git log
  ```

## Understanding .gitignore File 🚫

The `.gitignore` file is an essential tool in Git. It tells Git which files or folders to ignore in a project. This is particularly useful for excluding files that don't need to be tracked or are generated during the development process, like log files, build outputs, or files containing sensitive information.

### Purpose of .gitignore

- **Excluding Unnecessary Files:** Prevents adding temporary files, build artifacts, logs, etc., to the repository.
- **Avoiding Sensitive Information:** Helps in keeping sensitive data like configuration files with credentials out of the repository.
- **Cleaner Commits:** Ensures that your commits contain only relevant changes and are not cluttered with unnecessary files.

### Creating and Configuring .gitignore

- **Creating .gitignore:** Typically, `.gitignore` is placed in the root of your repository. You can create it manually or through the command line: `touch .gitignore`.
- **Configuring .gitignore:** Add patterns of file names or directories to this file to tell Git what to ignore. For example:
  - `*.log` ignores all files with a `.log` extension.
  - `node_modules/` ignores a directory named `node_modules`.
  - `!important.log` includes `important.log` even if `.log` files are ignored.

### Best Practices

- **General Rules:** Include general rules for common unwanted files (like `*.tmp`) but be specific enough to not accidentally ignore necessary files.
- **Templates:** Utilize templates for `.gitignore`. A great resource for this is [Toptal's `.gitignore` template generator](https://www.toptal.com/developers/gitignore/), which offers ready-made templates for different environments and languages, simplifying the process of creating an effective `.gitignore` file.
- **Committing .gitignore:** It’s a good practice to commit the `.gitignore` file to your repository, so these rules are shared among all contributors.
- **Any file that has sensitive information should be omitted utilizing the `.gitignore`**, I use the same named files when developing to keep it easy; `secrets.json`,`api_keys.txt`, `garbage.txt`, and `wtf.txt`.

#### Example `.gitignore` file

```.gitignore
api_keys.txt
secrets.json
garbage.txt
wtf.txt

### macOS ###
# General
.DS_Store
.AppleDouble
.LSOverride

### VisualStudioCode ###
.vscode/*
!.vscode/settings.json
!.vscode/tasks.json
!.vscode/launch.json
!.vscode/extensions.json
!.vscode/*.code-snippets

### Windows ###
# Windows thumbnail cache files
Thumbs.db
Thumbs.db:encryptable
ehthumbs.db
ehthumbs_vista.db

### AND SO MANY MORE! ###
```

You should now be able to do the basics of utilizing git. Lesson two will go deeper into merging your code, remediating conflicts, and other cool things like that..
