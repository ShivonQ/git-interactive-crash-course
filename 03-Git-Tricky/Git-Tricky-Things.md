# Lesson 3: Git Tricky

[[_TOC_]]

## Introduction to Advanced Git Commands 🚀

Welcome to the third lesson of our Git series! This lesson covers some powerful, yet less commonly used Git commands. Understanding these commands will enhance your capability to manage complex scenarios in Git. So buckle up mother-truckers.

## Git Reset: Rewinding Changes 🔄

`git reset` is a powerful command used to undo local changes to the state of a Git repository. It's a tool for undoing changes and can be both incredibly useful and potentially dangerous if not used correctly.

### Understanding HEAD in Git

Before diving into `git reset`, it's important to understand what HEAD is:

- **HEAD** is a reference to the last commit in the currently checked-out branch.
- It represents the current state of the repository and is what you see in your working directory.
- Understanding HEAD is crucial because `git reset` essentially moves this HEAD pointer to a different commit.

### Commit vs. Merge: Clarifying the Confusion

- **Commit:** A commit is an individual change to a file (or set of files). It's like taking a snapshot of your repository at a specific moment in time. When you commit, you are recording the state of your repository into its history.
- **Merge:** A merge, on the other hand, is the act of integrating changes from one branch into another. It’s about combining the histories of different branches.
- **Remember:** Committing is a part of the day-to-day workflow in Git, allowing you to track progress and create checkpoints, while merging is a step that usually comes later, to integrate completed work.

### When and Why to Use Git Reset

- **Use Case:** Use `git reset` when you need to undo changes in your working directory and staging area.
- **Correcting Mistakes:** It's particularly helpful for correcting mistakes before they are committed or for removing unwanted changes from the staging area.
- **Caution:** Be cautious with `git reset`, especially with the `--hard` option, as it can permanently delete your uncommitted changes.

### Types of Git Reset

1. **`git reset --soft`**

   - **What It Does:** Moves HEAD to another commit but leaves your working directory and index (staging area) as-is.
   - **Use Case:** When you want to undo the commit but keep the changes in your staging area.

2. **`git reset --mixed` (Default)**

   - **What It Does:** Resets the index but does not touch the working tree (files).
   - **Use Case:** When you want to undo the commit and changes in the staging area but keep the edited files in your working directory.

3. **`git reset --hard`**
   - **What It Does:** Resets the index and working tree. Any changes to tracked files in the working directory since the specified commit are discarded.
   - **Use Case:** When you want to completely undo changes, both committed and uncommitted.
   - **Warning:** This is the most dangerous option, as it can result in irreversible loss of uncommitted changes.

If you are _really_ interested in learning more about how git reset really works, then I recommend [this section of the manual](https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified) to be read. It goes into **GREAT** detail about the inner workings of git itself, and is an interesting read but dense.

#### Git Reset Visualization:

![A visualization of the movements on the git tree, showing how the most common commands function in relation to "staging/Index" and other components of the actual mechanics of git.](/assets/git-actions.jpeg)

## Git Stash: Saving Changes Temporarily 📥

`git stash` temporarily shelves (or stashes) changes you've made to your working copy.

### Understanding Git Stash

`git stash` is a versatile command used to temporarily store modified, tracked files in order to return to a clean working directory. It’s particularly useful when you need to switch contexts without committing incomplete work.

#### Creating a Stash

- **Basic Stash:** To stash changes, simply use:

  ```bash
  git stash
  ```

  This command stashes changes in tracked files and staged changes, reverting your working directory to the last commit state.

- **Naming a Stash:** For better organization, you can name your stashes for future reference:
  ```bash
  git stash save "My Stash Name"
  ```
  This is useful for identifying stashes later, especially if you have multiple stashes.

#### Applying Stashes

- **Applying the Latest Stash:**

  ```bash
  git stash apply
  ```

  This command re-applies the changes from the most recent stash without removing them from the stash list.

- **Applying a Specific Stash:**
  First, list all your stashes:
  ```bash
  git stash list
  ```
  Then apply a specific stash by its identifier (found in the stash list):
  ```bash
  git stash apply stash@{stash_number}
  ```

#### Managing Stash List

- **Removing a Single Stash:**
  After applying a stash, you might want to remove it from the stash list:

  ```bash
  git stash drop stash@{stash_number}
  ```

- **Clearing All Stashes:**
  To remove all stashed changes, use:
  ```bash
  git stash clear
  ```
  Be cautious with this command, as it permanently deletes all stashed changes.

---

## Git Diff: Exploring Changes 🕵️

`git diff` is a command used to display differences in Git, offering a crucial way to review changes before committing them. It can show differences between file versions, commits, and branches.

### Understanding Git Diff

- **What is a Diff?**

  - A 'diff' is a comparison between two states of a file or set of files. It shows what has been added or removed, line by line.
  - In Git, `diff` helps you see changes between commits, the current state of your files, and the last commit, or even differences between branches.

- **Use Cases for Git Diff:**
  - **Before Staging:** To view changes in your working directory that are not yet staged.
  - **Between Commits:** To compare changes between different commits.
  - **Branch Comparisons:** To see how a branch has diverged from another, like the main branch.

### Using Git Diff on the Command Line

- While `git diff` can be used directly in the command line, it can sometimes be hard to read, especially for large changes or multiple files.

  ```bash
  git diff                  # View changes in the working directory
  git diff --staged         # View changes that are staged but not committed
  git diff [commit1] [commit2]  # Compare two commits
  ```

### Viewing Diffs in GitLab or VSCode

- **GitLab:**

  - GitLab provides a user-friendly interface to view diffs. It visually represents changes in merge requests or between commits, highlighting added and removed lines.
  - This visual representation is especially useful when reviewing code for merge requests, making it easier to understand the context and impact of changes.
  - You can create Draft Merge Requests for this very purpose, to just see what has changed.

- **VSCode:**
  - VSCode also has excellent support for viewing diffs. When you open a Git repository in VSCode, you can easily see changes in each file.
  - VSCode's diff viewer splits the screen to show the current state of the file on one side and the modified state on the other, enabling an easy-to-follow side-by-side comparison.

#### GitLab Diff View:

- You literally just do all your `commit`s and push your branch up.
- In the Merge Request, you check the checkbox labeled "Mark as a draft MR".
- You can now navigate to the "Changes" tab and see what is different b/t the branches you are comparing.

---

## Visualizing History with Git Log --graph 📊

`git log --graph` provides a visual representation of the commit history.

### Purpose of Git Log --graph

- This command is used to see the branching and merging history in a graphical way.
- It helps in understanding the flow of commits, especially in a project with multiple branches.

#### `git log --graph` output

Inside of this expander you will find the output from this very repository on the `git log --graph` command. This _CAN_ be useful when trying to figure out whats wrong with git, I personaly do not use it very often. But if you have a lot of devs working on the same application (think of any website basically) this can be helpful in figuring out merge conflicts, and where your own origination point within the overall git tree is.

<details>
  <summary>git log --graph output inside!</summary>
  
  ### Example output from this command:

```bash
*   commit 64cc692200f8a47f9de4027697a230df2e8ecb15 (origin/feature/add-gitignore-section, feature/add-gitignore-section)
|\  Merge: 8f9224e f0b7ad8
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 23:55:17 2023 +0000
| |
| |     Merge branch 'feature/lesson-3' into 'main'
| |
| |     added a little more detail to the first less, due to git reset work.
| |
| |     See merge request bluestem/toolbox/git-crash-course!4
| |
| * commit f0b7ad8a54c96b022983af99d15fccbf4ac97ee1 (origin/feature/lesson-3, feature/lesson-3)
|/  Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
|   Date:   Wed Nov 15 17:54:45 2023 -0600
|
|       added a little more detail to the first less, due to git reset work.
|
*   commit 8f9224e3afa974c701f1a8f6b8473a1411dfe0b5
|\  Merge: 2e690f6 fd24e32
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 23:14:01 2023 +0000
| |
| |     Merge branch 'feat/git-and-others' into 'main'
| |
| |     feat: added most of the next lesson, working on the MR section.
| |
| |     See merge request bluestem/toolbox/git-crash-course!3
| |
| * commit fd24e32acbada5ccb0fa7e36a363cb940a683dd8 (origin/feat/git-and-others, feat/git-and-others)
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 17:13:36 2023 -0600
| |
| |     feat: finished lesson 2 on git
| |
| * commit f3ca13efccdcfd1797794304877ecedd1b5f386f
|/  Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
|   Date:   Wed Nov 15 16:07:18 2023 -0600
|
|       feat: added most of the next lesson, working on the MR section.
|
*   commit 2e690f62634cd2a4d2c8e547e0b631c9e5a362aa
|\  Merge: e5cc363 4345a19
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 16:23:18 2023 +0000
| |
| |     Merge branch 'feat/add-intro' into 'main'
| |
| |     added some more details
| |
| |     See merge request bluestem/toolbox/git-crash-course!2
| |
| * commit 4345a199f4a448f616f3662ff20a41e5c1c91a49 (origin/feat/add-intro, feat/add-intro)
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 10:20:18 2023 -0600
| |
| |     added some more details
| |
* | commit e5cc363fce88b1dc88c8f869751875d377c10764
|\| Merge: ac2e254 4a91eb8
| | Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
| | Date:   Wed Nov 15 16:22:19 2023 +0000
| |
| |     Merge branch 'feat/add-intro' into 'main'
| |
| |     feat: added the intro config and basics.
| |
| |     See merge request bluestem/toolbox/git-crash-course!1
| |
| * commit 4a91eb84ca87aab6b4dbbc47d81e53ee31fe59dd
|/  Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
|   Date:   Wed Nov 15 08:57:35 2023 -0600
|
|       feat: added the intro config and basics.
|
* commit ac2e254a50f3efc87fccd9db35ec5e905cf4283f
  Author: Malcolm Leehan <malcolm.leehan@bluestembrands.com>
  Date:   Tue Nov 14 21:14:44 2023 +0000
```

</details>
