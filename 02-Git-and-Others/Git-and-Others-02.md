# Lesson 2: Branching, Merging, and Collaboration in Git

[[_TOC_]]

## Introduction to Advanced Git Techniques 🌟

Welcome to the second lesson in our Git Crash Course! This lesson will delve into more advanced Git techniques such as branching and merging, which are essential for collaborative development. We'll also explore how Git facilitates effective collaboration among developers.

## Understanding Branching in Git 🌿

Branching is a core concept in Git that allows multiple developers to work on different features simultaneously without affecting the main codebase.

### What is a Branch?

A branch in Git represents an independent line of development. Branches allow you to diverge from the main codebase, make changes, and then bring those changes back into the main project at a later time.

A great breakdow of branching and all it can do is found [here](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell).

#### Working with branches:

![A visual depiction of using branches.  You can see a string of commits (green dots) on the diagram, in each branch.](/assets/git-feature-branches.svg)

### Creating and Switching Branches

Learn how to create and switch between branches in Git with practical commands.

```bash
$ git branch testing-branches
$ git checkout testing-branches

OR

$ git checkout -b testing-branches
```

_These commands should have created a branch called `testing-branches`. You can view your current branches with the following command._

```bash
git branch
```

_Be warned, this will open the view of your branches in your commandline editor (`vi` in my case)._

### **Hands On Task: Branch Management Exercise 🌳**

Practice cleaning up your local branches. Make a few branches with various names. Then do the following.

1. After merging a feature branch, delete it using: `git branch -d branch-name`.
2. Verify it's deleted with: `git branch`.

## The Power of Merging 🔄

Merging is the process of integrating changes from one branch into another. It's a critical aspect of collaborative work in Git.

### Understanding Merge

Merging combines the changes from one branch (feature branch) into another (usually the main branch).

#### A simple `$ git merge` diagram:

![A visual depiction of using merging in its most basic form.  Merging your branch with the main branch.](/assets/git-merge-dev.png)

## Merge/Pull Requests and Code Reviews

Merge/Pull Requests (PRs), also known as merge requests (MRs) in some platforms like GitLab, are essential features of Git hosting services like GitHub and GitLab. They play a central role in collaborative software development, enabling developers to propose, discuss, and review code changes.

### Creating and Reviewing Merge/Pull Requests

Creating a Merge/Pull Request is a way of informing team members that you have completed a feature or fix that you want to merge into a main branch. This process facilitates code reviews, discussions, and additional changes before the code is integrated.

This exercise will guide you through the process of using Git for collaboration, emphasizing the use of branches, Merge/Pull Requests, and the code review process.

### Hand On Task: Create an MR, and Merge Code:

1. **Create a New Branch:**  
   Start by creating a branch for the changes you plan to make.

   ```bash
   git checkout -b feature/my-new-feature
   ```

2. **Make Changes and Commit Them:**  
   Implement your changes and commit them to your branch.

   ```bash
   git add .
   git commit -m "Add a new feature"
   ```

3. **Push the Branch to the Remote Repository:**  
   Push your new branch to the remote repository.

   ```bash
   git push origin feature/my-new-feature
   ```

4. **Create a Merge/Pull Request:**  
   Go to the repository on GitHub/GitLab and create a new Merge/Pull Request for your branch.

   You commandline may also provide you with a direct URL link for your MR/PR after the `push` command executes.

5. **Merge the Merge/Pull Request After Review:**  
   After your Merge/Pull Request is reviewed and approved by your teammates, merge it into the main branch.

### Handling Merge Conflicts

Merge conflicts occur when Git can’t automatically resolve differences in code between two commits.

This happens all the time and you should get some clues from git when they have happened. As a matter of fact, git will raise up a Conflict error, and then even make changes in the files for where the conflict is. Let's take a look.

```bash
git merge branch_name main

Auto-merging [file_name]
CONFLICT (content): Merge conflict in [file_name]
Automatic merge failed; fix conflicts and then commit the result.
```

And then inside of your `[file_name]` that the conflict has arisen in, you should see some text that git added.

I did add a `the-test-text-file.txt` to this repo for the purposes of messing with it while you do these excersises.

```
<<<<<< HEAD
[Your changes in the current branch]
=======
[Conflicting changes from the branch you are trying to merge]
>>>>>> [branch-name]
```

_The real version of this message uses 7 `<` characters and not 6, but if I use 7 then `git` picks it up as a merge conflict, forcing me to remediate it._

### Hands On Task: Merge Conflict Resolution 🛠

Please do the following in order to see a merge conflict in the real world.

1. Create a new branch: `git checkout -b conflict-branch`.
2. Make a change in the same line of the same file (`the-test-text-file.txt`) that exists on the main branch.
3. Commit the change: `git commit -am "Conflicting change"`.
4. Switch back to the main branch: `git checkout main`.
5. Make a different change on the same line of the same file (`the-test-text-file.txt`).
6. Commit this change: `git commit -am "Change on main"`.
7. Try to merge `conflict-branch` into `main` with the following command: `git merge conflict-branch`.
8. Resolve the conflict by editing the file, then commit the resolution.

#### MR's In GitLab

The MR Process is as follows.

1. So, I have gone through all of my `git` commands to `push` my code up, after making my code changes, and I have just used `git push origin my-branch-name`.

---

## ![You can see I have pushed my code up to gitlab, and it spits out a link for the MR.](/assets/git-push-result.png)

#### Alternatively navigate to your repo and select "Merge Requests" in the UI.

![See the merge requests ui option and click it.](/assets/gitlab-mr-ui-option.png)

2. Now we go to that link or click "Create merge request". You should be able to now edit the description of your MR, mark it as a draft (very helpful when collaborating with others), assign reviewers, labels and much more. 90% of those things dont matter.

## ![See the merge requests ui option and click it.](/assets/gitlab-make-mr-ui.png)

- Your bare minimum is a description of your MR, reviewers, and the ☑️ for `Delete source branch when merge request is accepted.` You want that to occur, it helps keep your GitLab workspace clean (this does not remove it locally, so even if you need it you still have it.)

3. Finally we can actually perform the merge by clicking the merge button. If this repo had specific approval rules, it would very clearly state that right here on this MR. As long as there are no MERGE CONFLICTS then the merge will just happen.

## ![See the merge requests ui option and click it.](/assets/gitlab-mr-merge-me.png)

4. Now you can go ahead and run `git checkout main` or `git checkout master` depending on the project, and the run `git pull` and you should now see your changes in the `main/master` branch that you just refreshed.

### The Role of Code Reviews in PRs

Code reviews in PRs are not just about finding errors. They serve as an opportunity for knowledge sharing, improving code quality, and ensuring consistency in the project's codebase.

#### Visual Aid Placeholder:

- **Type:** Screenshots or Annotations
- **Description:** Screenshots of a code review in progress, showing comments, suggestions, and discussions among team members.

## Collaborating with Others: A Hands-On Exercise

So now we are going to try makeing MRs to others and having them make them to your repo to see how the code review process/merge process works.
You will need to do the following:

1. Pick a persons repo, and clone their repo down (lesson 1).
1. Make your branch in their repo, locally.
1. Change the text in `the-test-text-file.txt` to whatever you want (or make other changes, you're an adult and I can't tell you what to do.)
1. Run your git commands, `add` and `commit`.
1. Run your push command.
1. Create the MR and ask for the owner to review it.
1. Resolve any conflicts that may come about.
1. Merge that code in!

---

### Merging a Forked Repo with the Upstream Original Repo

This lesson we will walk through how to sync your own repository with any upstream changes that I may have made in my own iteration of the repository. Like this change right here in fact.

#### 1. **Fork the Repository**

- Initially, you fork the original repository. This creates a copy of the repository under your GitHub account.

#### 2. **Clone Your Fork Locally**

- Clone your forked repository to your local machine:
  ```bash
  git clone https://github.com/your-username/repo-name.git
  ```
- Navigate into the cloned directory:
  ```bash
  cd repo-name
  ```

#### 3. **Add the Upstream Repository**

- Add the original repository as a remote (often called "upstream") to your local repository. This step allows you to fetch changes from the original repository:
  ```bash
  git remote add upstream https://github.com/original-username/repo-name.git
  ```
- Verify that the new remote is added:
  ```bash
  git remote -v
  ```

#### 4. **Fetch Upstream Changes**

- Fetch changes from the upstream repository:
  ```bash
  git fetch upstream
  ```

#### 5. **Merge Upstream Changes into Your Fork**

- Check out the branch you want to merge the changes into (usually the main or master branch):
  ```bash
  git checkout main
  ```
- Merge the changes from the upstream's main branch into your local branch:
  ```bash
  git merge upstream/main
  ```
- Resolve any conflicts that arise and commit the merge.

#### 6. **Push Merged Changes to Your Forked Repository**

- Push the merged changes back to your forked repository on GitHub:
  ```bash
  git push origin main
  ```

#### Best Practices and Tips

- **Stay Updated:** Regularly fetch and merge changes from the upstream repository to keep your fork up-to-date.
- **Handle Conflicts:** If there are conflicts during the merge, carefully resolve them before finalizing the merge.
- **Work in Branches:** For significant changes or contributions, it's recommended to work in a separate branch rather than directly on the main branch.

This process ensures that your fork is synchronized with the latest changes from the original repository. It's a common practice in open-source projects where many contributors work on different forks of the same repository.
